<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_cliente',20);
            $table->string('nome',100);
            $table->string('fantasia',100);
            $table->date('data_fundacao');
            $table->enum('tipo_cliente');
            $table->string('cnpj',14);
            $table->string('cpf',11);
            $table->integer('id_endereco')->unsigned();
            $table->foreign('id_endereco')->references('id')->on('endereco')->OnDelete('cascade');
            $table->integer('id_contato')->unsigned();
            $table->foreign('id_contato')->references('id')->on('contato')->OnDelete('cascade');
            $table->boolean('ativo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
